/*
* ---------------------------------------------------------------------
* Copyright (c) 2017      Raheman Vaiya
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
* ---------------------------------------------------------------------
*/


#include <sys/types.h>
#include <regex.h>
#include <X11/Xlib-xcb.h>
#include <xcb/xcb.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "color.h"
#include "key.h"
#include "util.h"
#include "font.h"
#include "cfg.h"
#include "xft.h"
#include "textbox.h"

#ifdef DEBUG
#define die(fmt, ...) _die("%s: (line %d): "fmt, __func__, __LINE__,  ##__VA_ARGS__)
#else
#define die(fmt, ...) _die(fmt, ##__VA_ARGS__)
#endif

#define screen(con) (xcb_setup_roots_iterator(xcb_get_setup(con)).data)

xcb_screen_t *screen;
xcb_connection_t *con;
Display *dpy;

struct menu_ctx {
  xcb_gcontext_t rect_gc;
  xcb_gcontext_t sel_rect_gc;
  struct xft_font_drw *font;
  struct xft_font_drw *sel_font;
  xcb_window_t win;
  const char *fgcol;
  const char *bgcol;
  const char *fontname;
  uint32_t padding;
  uint32_t spacing;
  uint32_t sel;
  uint32_t page_sz;
  uint32_t winh;
  const char **page;
  const char **items;
  char *last_search;
  size_t items_sz;
  struct textbox *qbox;
};

enum alignment {
  LEFT,
  CENTRE,
};

void _die(char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  
  vfprintf(stderr, fmt, ap);
  
  xcb_disconnect(con);
  va_end(ap);
  exit(-1);
}

void grab_kbd(xcb_connection_t *con, xcb_window_t win) {
  xcb_grab_keyboard_reply_t *r;
    xcb_generic_error_t *err = NULL;
  r = xcb_grab_keyboard_reply(con, xcb_grab_keyboard(con,
                                                     0,
                                                     win,
                                                     XCB_CURRENT_TIME,
                                                     XCB_GRAB_MODE_SYNC,
                                                     XCB_GRAB_MODE_ASYNC), &err);
                              
  while (r->status != XCB_GRAB_STATUS_SUCCESS) {
    r = xcb_grab_keyboard_reply(con, xcb_grab_keyboard(con,
                                                       0,
                                                       win,
                                                       XCB_CURRENT_TIME,
                                                       XCB_GRAB_MODE_SYNC,
                                                       XCB_GRAB_MODE_ASYNC), NULL);
  }
}

void parse_field_range(const char *_str, int *lower, int *upper) {
  char *str = strdup(_str);
  char *sep = strchr(str, '-');
  if(!sep) {
    *lower = atoi(str);
    *upper = *lower;
    free(str);
    return;
  }
  
  *sep = '\0';
  *lower = atoi(str);
  *upper = atoi(sep+1);
  if(*(sep + 1) == '\0')
    *upper = INT_MAX;
  free(str);
}

static char *fields(const char *str, char delim, int start, int end) {
  int c = 0;
  int i = 0;
  char *result = NULL;
  
  for(;*str;str++) {
    if(c >= (start-1) && c <= (end-1)) {
      if(!result)
        result = malloc(strlen(str) + 1);
      if(c == (end-1) && *str == delim) {
        result[i] = '\0';
        return result;
      }
      result[i++] = *str;
    }
    if(*str == delim) c++;
  }
  
  if(c == (end-1) && !result) {
	  result = malloc(1);
	  *result = '\0';
  }

  if(result)
    result[i] = '\0';
  return result;
}


char **field_map(const char **items, size_t items_sz, char delim, int start, int end) {
  char **result = malloc(sizeof(char*) * items_sz);
  size_t i;
  for (i = 0; i < items_sz; i++) {
    result[i] = fields(items[i], delim, start, end);
    if(!result[i]) {
      free(result);
      return NULL;
    }
  }
  return result;
}

/* TODO make this UTF8 sensitive. */
static void replace_tabs(char **oline) {
  char *line;
  int n = 0;
  char *start;
  line = *oline;
  
  for(;*line;line++)
    if(*line == '\t')
      n++;
  
  start = realloc(*oline, strlen(*oline) + n * 4 + 1);
  line = start;
  for(;*line;line++)
    if(*line == '\t') {
      memmove(line + 4, line + 1, strlen(line));
      memset(line, ' ', 4);
    }
  
  *oline = start;
}


static xcb_gcontext_t rectgc(const char *color) {
  uint32_t col;
  
  col = hexcol(con, color, NULL);
  
  xcb_gcontext_t id = xcb_generate_id(con);
  xcb_create_gc(con, id, screen->root,
                XCB_GC_FOREGROUND |
                XCB_GC_BACKGROUND |
                XCB_GC_FILL_STYLE,
                (uint32_t[]){ col, col, XCB_FILL_STYLE_SOLID });
  return id;
}
  
static struct menu_ctx *menu_ctx(xcb_window_t win,
                                 char *fgcol,
                                 char *bgcol,
                                 char *sel_fgcol,
                                 char *sel_bgcol,
                                 char *font,
                                 int padding,
                                 int spacing,
                                 int width,
                                 const char **items,
                                 size_t items_sz) {
  size_t el_height, maxwinh;
  size_t tboxh = textbox_height(dpy, font);
  struct menu_ctx *ctx = malloc(sizeof(struct menu_ctx));
  
  struct geom rootdim = win_geom(con, screen->root);
  
  ctx->rect_gc = rectgc(bgcol);
  ctx->font = xft_get_font_drw(dpy, win, font, fgcol);
  
  ctx->sel_rect_gc = rectgc(sel_bgcol);
  ctx->sel_font = xft_get_font_drw(dpy, win, font, sel_fgcol);
  
  ctx->bgcol = bgcol;
  ctx->fgcol = fgcol;
  ctx->fontname = font;
  
  ctx->win = win;
  ctx->padding = padding;
  ctx->spacing = spacing;
  ctx->page = NULL;
  
  ctx->items = items;
  ctx->items_sz = items_sz;
  
  el_height = ctx->font->maxheight + (2 * ctx->padding) + ctx->spacing;
  maxwinh = rootdim.height - tboxh;
  
  ctx->last_search = NULL;
  ctx->page_sz = el_height * items_sz > maxwinh ? maxwinh / el_height : items_sz;
  ctx->winh = ctx->page_sz * el_height;
  
  ctx->qbox= textbox_init(dpy,
                          screen->root,
                          rootdim.width - width, ctx->winh,
                          ctx->fgcol,
                          ctx->bgcol,
                          ctx->fontname,
                          NULL,
                          width,
                          1);
  
  return ctx;
}

static void draw_rectangle(xcb_window_t win,
                           int x, int y,
                           int width, int height,
                           xcb_gcontext_t gc) {
  xcb_poly_fill_rectangle(con, win, gc, 1, (xcb_rectangle_t[]){{x, y, width, height}});
}

static void add_item(const char *item,
                     int pos,
                     int selected,
                     enum alignment alignment,
                     struct menu_ctx *ctx) {
  int height;
  int yoffset;
  struct geom txt;
  struct geom wgeom;
  xcb_gcontext_t rect_gc;
  struct xft_font_drw *font;
  uint32_t xoffset;
  
  size_t len;
  char *disp_item;
  
  disp_item = strdup(item);
  replace_tabs(&disp_item);
  len = strlen(disp_item);
  
  txt = xft_text_geom(ctx->font, disp_item, strlen(disp_item));
  wgeom = win_geom(con, ctx->win);
  height = ctx->font->maxheight + (2 * ctx->padding);
  rect_gc = selected ? ctx->sel_rect_gc : ctx->rect_gc;
  font = selected ? ctx->sel_font : ctx->font;
  yoffset = (height + ctx->spacing) * pos;
  
  xoffset = (alignment == CENTRE) ?
    (wgeom.width - txt.width) / 2 : 0;
  
  draw_rectangle(ctx->win, 0,
                 yoffset,
                 wgeom.width, height, rect_gc);

  /* Leave the original string intact for search purposes. */
  xft_draw_text(font,
                xoffset,
                yoffset + ((height - txt.height) / 2),
                disp_item, len);
  free(disp_item);
}

static void draw_page(const char **page, size_t sz, int sel,
                      struct menu_ctx *ctx) {
  int i;
  ctx->page = page;

  ctx->sel = sel;
  for (i = 0; i < (int)sz; i++)
    add_item(page[i], i, (i == sel), LEFT, ctx);
  
  xcb_flush(con);
  XFlush(dpy);
}

  
static void menu_update(const char **page, size_t sz, uint32_t sel, struct menu_ctx *ctx) {
  if(!ctx->page)
    ctx->page = page;

  if(page == ctx->page) {
    if(sel == ctx->sel)
      return;
    add_item(page[ctx->sel], ctx->sel, 0, LEFT, ctx);
    add_item(page[sel], sel, 1, LEFT, ctx);
    ctx->sel = sel;
  } else {
    /* If the page has changed redraw everything. */
    draw_page(page, sz, sel, ctx);
  }
  
  xcb_flush(con);
  XFlush(dpy);
}

static void init_con() {
  dpy = XOpenDisplay(NULL);
  con = XGetXCBConnection(dpy);
  XSetEventQueueOwner(dpy, XCBOwnsEventQueue);
  
  const xcb_setup_t *setup = xcb_get_setup(con);
  xcb_screen_iterator_t iter = xcb_setup_roots_iterator(setup);
  screen = iter.data;
}

static xcb_window_t create_win(int x, int y, int width, int height, uint32_t color) {
  xcb_window_t id = xcb_generate_id(con);
  xcb_create_window(con,
                    XCB_COPY_FROM_PARENT,
                    id,
                    screen->root,
                    x, y,
                    width, height,
                    0,
                    XCB_WINDOW_CLASS_INPUT_OUTPUT,
                    screen->root_visual,
                    XCB_CW_BACK_PIXEL | XCB_CW_OVERRIDE_REDIRECT | XCB_CW_EVENT_MASK,
                    (uint32_t[]){color, 1,
                        XCB_EVENT_MASK_EXPOSURE |
                        XCB_EVENT_MASK_KEY_PRESS |
                        XCB_EVENT_MASK_FOCUS_CHANGE});

  return id;
}

static void show_item(struct menu_ctx *ctx,
                      const char ***opage,
                      int *osel,
                      int target) {
  const char **page = (const char **) *opage;
  int sel = *osel;
  
  int start = page - ctx->items;
  int end = start + ctx->page_sz - 1;
  
  if(target < 0 || target >= (int)ctx->items_sz)
    return;
  
  if(target >= start && target <= end)
    sel = target - start;
  else if((ctx->items_sz - (target - ((int)ctx->page_sz / 2))) < ctx->page_sz) {
    page = ctx->items + ctx->items_sz - ctx->page_sz;
    sel = target - (ctx->items_sz - ctx->page_sz);
  } 
  else if((target - ((int)ctx->page_sz / 2)) < 0) {
    page = ctx->items;
    sel = target;
  }
  else {
    sel = (ctx->page_sz / 2);
    page = ctx->items + target - (ctx->page_sz / 2);
  }
  
  *opage = page;
  *osel = sel;
}

static void search_reverse(struct menu_ctx *ctx,
                           const char *pattern,
                           const char ***opage,
                           int *osel) {
  
  if(!pattern) return;
  
  const char **page = (const char **) *opage;
  int sel = *osel;
  int i;
  
  regex_t pat;
  
  if(regcomp(&pat, pattern, REG_NOSUB | REG_ICASE))
    return;
  
  for (i = sel + (page - ctx->items) - 1; i >= 0; i--) {
    if(!regexec(&pat, ctx->items[i], 0, NULL, 0)) {
      show_item(ctx, opage, osel, i);
      break;
    }
  }
  
  regfree(&pat);
}
static void search_forward(struct menu_ctx *ctx,
                           const char *pattern,
                           const char ***opage,
                           int *osel) {
  
  if(!pattern) return;
  
  const char **page = (const char **) *opage;
  int sel = *osel;
  int i;
  
  regex_t pat;
  
  if(regcomp(&pat, pattern, REG_NOSUB | REG_ICASE))
    return;
  
  for (i = sel + (page - ctx->items) + 1; i < (int)ctx->items_sz; i++) {
    if(!regexec(&pat, ctx->items[i], 0, NULL, 0)) {
      show_item(ctx, opage, osel, i);
      break;
    }
  }
  
  regfree(&pat);
}

static void isearch(struct menu_ctx *ctx,
                    const char ***opage,
                    int *osel,
                    int forward) {
  
  if(ctx->last_search) {
    free(ctx->last_search);
    ctx->last_search = NULL;
  }
  
  char *query = textbox_query(ctx->qbox, NULL, NULL, 0);
  
  if(!query) return;

  ctx->last_search = query;
  if(forward)
    search_forward(ctx, query, opage, osel);
  else
    search_reverse(ctx, query, opage, osel);
}

int menu(const struct cfg *cfg, const char **menu_items, const char **items, size_t items_sz, int init_search) {
  struct geom root;
  xcb_window_t win;
  struct menu_ctx *ctx;
  struct keymap *keymap;
  xcb_generic_event_t *ev;
  uint32_t bgcol;
  int sel;
  /* The maximum number of elements that will fit in the window. 
     (Bottleneck is the screen size). */
  int opnum = 0;
  const char **page = menu_items;
    
  keymap = get_keymap(con);
 
  root = win_geom(con, screen->root);
  bgcol = hexcol(con, cfg->bgcol, NULL);
  win = create_win(root.width - cfg->width, 0, cfg->width, root.height, bgcol);

  ctx = menu_ctx(win,
                 cfg->fgcol,
                 cfg->bgcol,
                 cfg->sel_fgcol,
                 cfg->sel_bgcol,
                 cfg->font,
                 cfg->padding,
                 cfg->spacing,
                 cfg->width,
                 menu_items,
                 items_sz);
 

  xcb_configure_window(con, win, XCB_CONFIG_WINDOW_HEIGHT, (uint32_t[]){ctx->winh});
  xcb_map_window(con, win);
  xcb_flush(con);
  
  grab_kbd(con, win);
  
  sel = 0;
  
  if(init_search) {
    draw_page(page, ctx->page_sz, sel, ctx);
    isearch(ctx, &page, &sel, 1); 
    draw_page(page, ctx->page_sz, sel, ctx);
  }
  
  char *last_keyname = NULL;
  while((ev = xcb_wait_for_event(con))) {
    char *keyname;
    
    enum {
      FORWARD,
      REVERSE
    } search_direction;
    
    switch(ev->response_type) {
      case XCB_EXPOSE:
        draw_page(page, ctx->page_sz, sel, ctx);
        xcb_flush(con);
        break;
      case XCB_KEY_PRESS:
        keyname = key_name(keymap,
                           ((xcb_key_press_event_t*)ev)->detail,
                           ((xcb_key_press_event_t*)ev)->state);
        if(!keyname) continue;
        
        if(!strcmp(cfg->key_home, keyname)) {
          opnum = opnum ? opnum : 1;
          sel = opnum - 1;
          if((uint32_t)sel >= ctx->page_sz)
            sel = ctx->page_sz - 1;
          opnum = 0;
        }
        else if(!strcmp(cfg->key_middle, keyname)) {
          opnum = opnum ? opnum : 1;
          sel = (ctx->page_sz - 1) / 2;
          opnum = 0;
        }
        else if(!strcmp(cfg->key_last, keyname)) {
          opnum = opnum ? opnum : 1;
          
          if(((int)ctx->page_sz - opnum) < 0)
            sel = 0;
          else
            sel = ctx->page_sz - opnum;
          
          opnum = 0;
        }
        else if(!strcmp(cfg->key_up, keyname)) {
          opnum = opnum ? opnum : 1;
          
          sel -= opnum;
          if(sel < 0) {
            page -= sel * -1;
            sel = 0;
          }

          if(page < menu_items)
            page = menu_items;
          opnum = 0;
        }
        else if(!strcmp(cfg->key_down, keyname)) {
          opnum = opnum ? opnum : 1;
          sel += opnum;
          if((uint32_t)sel >= ctx->page_sz) {
            page += sel - ctx->page_sz + 1;
            if(page >= menu_items + items_sz - ctx->page_sz)
              page = menu_items + items_sz - ctx->page_sz;
            sel = ctx->page_sz - 1;
          }
          opnum = 0;
        }
        else if(!strcmp(cfg->key_sel, keyname)) {
          printf("%s\n", items[page - menu_items + sel]);
          return 0;
        }
        else if(!strcmp(cfg->key_page_down, keyname)) {
          opnum = opnum ? opnum : 1;
          page += ctx->page_sz * opnum;
          if(page > menu_items + items_sz - ctx->page_sz)
            page = menu_items + items_sz - ctx->page_sz;
          sel = 0;
          opnum = 0;
        }
        else if(!strcmp(cfg->key_page_up, keyname)) {
          opnum = opnum ? opnum : 1;
          page -= ctx->page_sz * opnum;
          if(page < menu_items)
            page = menu_items;
          sel = ctx->page_sz - 1;
          opnum = 0;
        }
        else if(!strcmp("slash", keyname)) {
          opnum = opnum > 0 ? opnum - 1 : opnum;
          search_direction = FORWARD;
          isearch(ctx, &page, &sel, 1); 
          while(opnum--)
            search_forward(ctx, ctx->last_search, &page, &sel);
          opnum = 0;
        }
        else if(!strcmp("question", keyname)) {
          opnum = opnum > 0 ? opnum - 1 : opnum;
          search_direction = REVERSE;
          isearch(ctx, &page, &sel, 0); 
          while(opnum--)
            search_reverse(ctx, ctx->last_search, &page, &sel);
          opnum = 0;
        }
        else if(!strcmp("n", keyname)) {
          int i;
          opnum = opnum ? opnum : 1;
          
          if(search_direction == FORWARD)
            for (i = 0; i < opnum; i++)
              search_forward(ctx, ctx->last_search, &page, &sel);
          else
            for (i = 0; i < opnum; i++)
              search_reverse(ctx, ctx->last_search, &page, &sel);
          
          opnum = 0;
        }
        else if(!strcmp("N", keyname)) {
          int i;
          opnum = opnum ? opnum : 1;
          
          if(search_direction == FORWARD)
            for (i = 0; i < opnum; i++)
              search_reverse(ctx, ctx->last_search, &page, &sel);
          else
            for (i = 0; i < opnum; i++)
              search_forward(ctx, ctx->last_search, &page, &sel);
          
          opnum = 0;
        }
        else if(!strcmp("G", keyname)) {
          if(opnum)
            show_item(ctx, &page, &sel, opnum - 1);
          else
            show_item(ctx, &page, &sel, ctx->items_sz - 1);
          
          opnum = 0;
        }
        else if(!strcmp("g", keyname)) {
          if(last_keyname && last_keyname[0] == 'g')
            show_item(ctx, &page, &sel, 0);
        }
        else if(!strcmp(cfg->key_quit, keyname))
          exit(-1);
        else if(!strcmp("0", keyname) ||
                !strcmp("1", keyname) ||
                !strcmp("2", keyname) ||
                !strcmp("3", keyname) ||
                !strcmp("4", keyname) ||
                !strcmp("5", keyname) ||
                !strcmp("6", keyname) ||
                !strcmp("7", keyname) ||
                !strcmp("8", keyname) ||
                !strcmp("9", keyname))
          opnum = (opnum * 10) + (keyname[0] & 0xf);
        
        menu_update(page, ctx->page_sz, (uint32_t)sel, ctx);
        xcb_flush(con);
        last_keyname = keyname;
        break;
    }
    free(ev);
  }
  
  pause();
  xcb_disconnect(con);
  return 0;

  
}

void print_keynames() {
  size_t i;
  char **lst;
  size_t sz;
  key_names(&lst, &sz);
  for (i = 0; i < sz; i++)
    printf("%s\n", lst[i]);
  
  exit(1);
}

#define help_die(fmt, ...) do { \
  die("\
A tiny X11 program which displays a menu of items corresponding to \n\
input lines and prints the selected one to STDOUT. Items are drawn \n\
from the provided file or STDIN if no arguments are provided.  Item \n\
length is limited to the width of the screen and truncated if longer. \n\
The configuration parameters described below can \n\
be placed in ~/.xmenurc \n\n\
\
Optional configuration parameters: \n\n\
\
  fgcol: The foreground color. (of the form #xxxxxx) \n\
  bgcol: The background color. (of the form #xxxxxx) \n\
  sel_fgcol: The foreground color of the selected element. (of the form #xxxxxx) \n\
  sel_bgcol: The background color of the selected element. (of the form #xxxxxx) \n\
  font: The xft font pattern to be used. \n\
  padding: The amount of padding \n\
  spacing: The amount of space between elements. \n\
  width: The total width of the selection window. \n\
  key_last: A key which selects the last visible element. \n\
  key_middle: A key which selects the middle element. \n\
  key_home: A key which selects the first element. \n\
  key_down: A key which selects the next element. \n\
  key_page_down: A key which scrolls down one page. \n\
  key_page_up: A key which scrolls up one page. \n\
  key_up: A key which selects the previous element. \n\
  key_quit: A key which exists the dialogue without printing any items. \n\
  key_sel: A key which closes the menu and prints the selected item to STDOUT. \n\n\
\
Arguments: \n\n\
\
  -h: Prints this help message. \n\
  -c: Prints the current configuration parameters. \n\
  -k: Prints a list of valid key names that can be used in the config file. \n\
  -d: Specifies a record separator which allows specific portions of each line to \n\
      be used as menu items. (similar to cut)\n\
  -f: When used in conjunction with -d specifies which field to display. \n\
      Note that the original line is output in its original form and \n\
      can be further dissected by the likes of cut. Field ranges of the form \n\
      N-M are also supported. \n\n\
"fmt, ##__VA_ARGS__);\
} while(0)

void print_cfg(struct cfg *c) {
  printf("fgcol: %s\n", c->fgcol);
  printf("bgcol: %s\n", c->bgcol);
  printf("sel_fgcol: %s\n", c->sel_fgcol);
  printf("sel_bgcol: %s\n", c->sel_bgcol);
  printf("font: %s\n", c->font);
  printf("padding: %d\n", c->padding);
  printf("spacing: %d\n", c->spacing);
  printf("width: %d\n", c->width);
  
  printf("key_last: %s\n", c->key_last);
  printf("key_middle: %s\n", c->key_middle);
  printf("key_home: %s\n", c->key_home);
  printf("key_down: %s\n", c->key_down);
  printf("key_up: %s\n", c->key_up);
  printf("key_page_down: %s\n", c->key_page_down);
  printf("key_page_up: %s\n", c->key_page_up);
  printf("key_quit: %s\n", c->key_quit);
  printf("key_sel: %s\n", c->key_sel);
  exit(1);
} 

char **read_items(size_t *sz, FILE *fp) {
  int c = 1;
  int n = 0;
  char **lines = malloc(c * sizeof(char*));
  
  char *line;
  size_t len;
  line = NULL;len = 0;
  while(getline(&line, &len, fp) > -1) {
    n++;
    if(n > c) {
      c = n * 2;
      lines = realloc(lines, sizeof(char*) * c);
    }
    
    len = strlen(line);
    if(len)
      line[len - 1] = '\0';
    lines[n - 1] = line;
    line = NULL;len = 0;
  }
  
  *sz = n;
  return lines;
}

struct options {
  int print_config;
  int print_help;
  int print_keys;
  int print_version;
  int start_search;
  int prompt_mode;

  char *prompt_str;
  char delim;
  int field_lower;
  int field_upper;
  FILE *file;
};

struct options opt_parse(int *argc, char ***argv) {
  char c;

  struct options opt = { 0 };
  while((c = getopt(*argc, *argv, "pvhsckd:f:")) != -1) {
    switch(c) {
      case 'p': //Prompt mode
        opt.prompt_mode++;
        opt.prompt_str = optarg;
        break;
      case 'v':
        opt.print_version++;
        break;
      case 'h':
        opt.print_help++;
        break;
      case 's':
        opt.start_search++;
        break;
      case 'c':
        opt.print_config++;
        break;
      case 'k':
        opt.print_keys++;
        break;
      case 'd':
        opt.delim = *optarg;
        break;
      case 'f':
        parse_field_range(optarg, &opt.field_lower, &opt.field_upper);
        break;
      case '?':
        fprintf(stderr, "ERROR: Invalid option %c\n\n", optopt);
        die("Usage: %s [ -h | -c | -k ] [-s] [<file>]]\n", **argv);
        break;
    }
  }

  *argc -= optind;
  *argv += optind;
  
  if(!opt.prompt_mode) {
    if(*argc) {
        opt.file = fopen(**argv, "r");
        if(!opt.file)
        help_die("ERROR: %s is not a valid input file.\n", **argv);
    } else
        opt.file = stdin;
  }

  return opt;
}


void prompt(char *str, struct cfg *cfg) {
  //TODO add prompt string to textbox.c
  xcb_screen_t *screen = screen(con);
  struct textbox* tbox = textbox_init(dpy,
                                      screen->root,
                                      screen->width_in_pixels - cfg->width, 0,
                                      cfg->fgcol,
                                      cfg->bgcol,
                                      cfg->font,
                                      NULL,
                                      cfg->width,
                                      1);
  char *result = textbox_query(tbox, NULL, NULL, 1);
  printf(result);
}

int main(int argc, char **argv) {
  char cfg_file[256];
  struct cfg *cfg;
  size_t nitems;
  const char **items;
  char **menu_items;
     
  strncpy(cfg_file, getenv("HOME"), 240);
  strcpy(cfg_file + strlen(cfg_file), "/.xmenurc");
  cfg = get_cfg(cfg_file, argv[0]);
    
  init_con();
  struct options opt = opt_parse(&argc, &argv);

  if(opt.prompt_mode) {
    prompt(opt.prompt_str, cfg);
    return 0;
  } 
  else if(opt.print_version)
    die("Version: "VERSION"\n\nWritten By: Raheman Vaiya\n");
  else if(opt.print_help)
    help_die();
  else if(opt.print_config)
    print_cfg(cfg);
  else if(opt.print_keys)
    print_keynames();
   
  items = (const char**)read_items(&nitems, opt.file);
  if(opt.delim && opt.field_upper) {
    menu_items = field_map(items, nitems, opt.delim, opt.field_lower, opt.field_upper);
    if(!menu_items)
      die("ERROR: Invalid input, all lines must have at least %d fields.\n", opt.field_upper);
  }
  else
    menu_items = items;
  
  menu(cfg,
       menu_items,
       items,
       nitems,
       opt.start_search);
     
  return 0;
}
